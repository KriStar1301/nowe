#include <stdlib.h>
#include <stdio.h>
#include "funkcje1.h"
#include <string.h>



struct dane
{
    float x[50];
    float y[50];
    float RHO[50];
};

struct stats
{
    float srd[2];
    float med[2];
    float odch[2];
};


int main()
{
    FILE *plik;

    struct dane wczytaj;

    struct stats statystki;

    if((plik = fopen("P0001_attr.rec", "a+")) == NULL)
    {
        printf("Blad otwarcia pliku.\n");
    }
    else
    {
        //wczytaj[n] = wczytwanie(plik);
        char lp[4] = "";
        int i = 0;
    
        while(fscanf(plik, "%s %f %f %f", lp, &wczytaj.x[i], &wczytaj.y[i], &wczytaj.RHO[i])>0)
        {
            printf("%s %f %f %f \n", lp, wczytaj.x[i], wczytaj.y[i], wczytaj.RHO[i]);
            i++;
        }

        printf("Nasz lp - %s\n", lp);

        if(strcmp(lp, "50.") == 0)
        {
            fprintf(plik, "Dane dla X:\nSrednia: %f\nMediana: %f\nOdchylenie standardowe: %f\n", srd(wczytaj.x), med(wczytaj.x), odch(wczytaj.x));
            fprintf(plik, "Dane dla Y:\nSrednia: %f\nMediana: %f\nOdchylenie standardowe: %f\n", srd(wczytaj.y), med(wczytaj.y), odch(wczytaj.y));
            fprintf(plik, "Dane dla RHO:\nSrednia: %f\nMediana: %f\nOdchylenie standardowe: %f\n", srd(wczytaj.RHO), med(wczytaj.RHO), odch(wczytaj.RHO));
            //printf("%f\n", wczytaj[49].x);
        }   
    }

    printf("Dane dla X:\nSrednia: %f\n", srd(wczytaj.x));
    printf("Mediana: %f\n", med(wczytaj.x));
    printf("Odchylenie standardowe: %f\n", odch(wczytaj.x));

    printf("Dane dla Y:\nSrednia: %f\n", srd(wczytaj.y));
    printf("Mediana: %f\n", med(wczytaj.y));
    printf("Odchylenie standardowe: %f\n", odch(wczytaj.y));

    printf("Dane dla RHO:\nSrednia: %f\n", srd(wczytaj.RHO));
    printf("Mediana: %f\n", med(wczytaj.RHO));
    printf("Odchylenie standardowe: %f\n", odch(wczytaj.RHO));


    statystki.srd[0] = srd(wczytaj.x);
    statystki.med[0] = med(wczytaj.x);
    statystki.odch[0] = odch(wczytaj.x);

    statystki.srd[1] = srd(wczytaj.x);
    statystki.med[1] = med(wczytaj.x);
    statystki.odch[1] = odch(wczytaj.x);

    statystki.srd[2] = srd(wczytaj.x);
    statystki.med[2] = med(wczytaj.x);
    statystki.odch[2] = odch(wczytaj.x);

    fclose(plik);

    //printf("%f", wczytaj[2].y);

    return 0;
}


