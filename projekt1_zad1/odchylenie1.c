#include "funkcje1.h"
#include <math.h>

float odch(float tab[])
{
    float sumisrd = 0.0;
    float odchylenie = 0.0;

    for(int i=0; i<50; i++)
    {
        sumisrd += tab[i];
    }

    sumisrd /= 50;

    for(int i=0; i<50; i++)
    {
        odchylenie += (tab[i] - sumisrd)*(tab[i] - sumisrd);
    }

    odchylenie /= 50;

    return sqrt(odchylenie);
}