#include <stdio.h>
#include <stddef.h>
#include "funkcje.h"

int main()
{
    float x[3] = {0.1, 0.4, 2.3};
    size_t elem = sizeof(x) / sizeof(x[0]);
    print("Wartosc sumy wynosi: %f\n", sum(x, elem));
    printf("Wartosc sredniej wynosi %f\n", mean(x, elem));
    getchar();
    return 0;
}